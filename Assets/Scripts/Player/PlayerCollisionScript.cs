﻿using UnityEngine;
using System.Collections;

public class PlayerCollisionScript : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Enemy")
        {
            // @TODO: Game Over Logic
            Destroy(gameObject);
        }
    }
}
